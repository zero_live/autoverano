describe('Verano', () => {
  beforeAll(() => {
    //executes before all the tests
  })

  beforeEach(() => {
    //executes before each tests
  })

  afterEach(() => {
    //executes after each tests
  })

  afterAll(() => {
    //executes after all tests
  })

  it('can use the following matchers', () => {
    expect(true).toBe(true)
    expect([1,2]).toBe([1,2])
    expect({a: 1, b: 2}).toBe({a: 1, b: 2})
    expect(() => { throw new Error() }).toThrowAnError()
    expect(() => { throw new Error() }).toThrowAnError(Error)
    expect([1,2]).include(1)
    expect('string').include('str')

    expect(undefined).toBeUndefined()
    expect(null).toBeNull()
    expect(true).toBeTruthy()
    expect(true).toBeFalsy()
  })

  it('can be negated', () => {
    expect(false).not.toBe(true)
  })

  xit('this test wont be executed')
})
