#!/bin/bash

kata_name=$1
zip_file="${kata_name}.zip"
production_file_name="${kata_name}.js"
test_file_name="${kata_name}Spec.js"

mkdir ${kata_name} &&
cd ${kata_name} &&
wget https://gitlab.com/devscola/verano/-/jobs/artifacts/master/download?job=build_job -nv -O ${zip_file} &&
unzip ${zip_file} &&
rm ${zip_file} &&
mv Verano/Verano.html . &&
rm -rf Verano &&
touch ${production_file_name} &&
wget https://gitlab.com/zero_live/autoverano/-/raw/master/ExamplesSpec.js?inline=false -O ${test_file_name} &&
echo "<script src="${production_file_name}"></script>" >> Verano.html &&
echo "<script src="${test_file_name}"></script>" >> Verano.html &&
xdg-open Verano.html
