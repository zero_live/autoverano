## AutoVerano

It's a simple script to prepare a test environment with [Verano Testing library](verano.devscola.org) [(by Devscola)](devscola.org) to do little things with JS. You can download the script clicking [here](https://gitlab.com/zero_live/autoverano/-/raw/master/AutoVerano.sh?inline=false) (Save the file with the name AutoVerano.sh)

## System requirements

- wget
- unzip

> Tested in Ubuntu with bash terminal.

## How to use

- Clone this repository in you local machine.
- Enter the project folder.
- Execute the following command:

`./AutoVerano.sh <your_project_name>`

This creates a folder with the name of your project with some test examples. And opens the test suit in your default browser.

After that just open the new folder with your text editor and refresh the browser page after save changes in your project.
